package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

public class uc1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uc1);

        Button btn = (Button) findViewById(R.id.button16);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LinearLayout lp = findViewById(R.id.layout01);
                lp.setOrientation(LinearLayout.HORIZONTAL);



            }
        });

        Button btn3 = (Button) findViewById(R.id.button17);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout lp2 = findViewById(R.id.layout01);
                lp2.setOrientation(LinearLayout.VERTICAL);
            }
        });


        Button btn4 = (Button) findViewById(R.id.button20);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout lp2 = findViewById(R.id.layout01);
                lp2.setGravity(3);//等价于Gravity.LEFT
            }
        });

        Button btn5 = (Button) findViewById(R.id.button22);
        btn5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Intent i = new Intent(uc1Activity.this , MainActivity.class);
                startActivity(i);
            }
        });

            }


    }
