package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import java.util.Timer;
import java.util.TimerTask;

public class uc3Activity extends AppCompatActivity {

    int []images = new int[]{R.drawable.a,R.drawable.b,
            R.drawable.c,R.drawable.d,R.drawable.e};
    int imagestart = 0;
    ImageView imageView;

    @SuppressLint("HandlerLeak")
    Handler handler =new Handler(){
        //重写handleMessage方法，根据msg中what的值判断是否执行后续操作
        public void handleMessage(Message msg){
            super.handleMessage(msg);
            if (msg.what==1){
                imageView.setImageResource(images[imagestart++ % 5]);
            }
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uc3);

        Button btn2 = (Button) findViewById(R.id.button6);
        btn2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Intent i = new Intent(uc3Activity.this , MainActivity.class);
                startActivity(i);
            }
        });


        imageView = findViewById(R.id.image_e);
        //使用定时器
        Timer timer =new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run(){
                handler.sendEmptyMessage(1);
            }
        } ,0,1000);
    }









    }
